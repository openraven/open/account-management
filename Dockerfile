FROM registry.gitlab.com/openraven/open/orvn-java:17

ARG JAR_FILE
ADD ${JAR_FILE} /root/service.jar
RUN if [ ! -f /root/service.jar ]; then \
        echo "Expected /root/service.jar to be a file" >&2 ;\
        ls -la /root/service.jar >&2 ;\
        exit 1 ;\
    fi
