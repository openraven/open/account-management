package io.openraven.properties.zookeeper;

import io.openraven.services.ZookeeperService;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@ConfigurationProperties(prefix = ScannerProperties.CONFIG_PATH)
public class ScannerProperties {
    public static final String CONFIG_PATH = "openraven.app.v1.scanner";
    public static final String ZK_PATH = CONFIG_PATH.replaceAll("\\.", "/");
    public static final String MAXIMUM_FILE_SIZE_UPPER_LIMIT_NAME = "/maximumfilesizeupperlimit";

    private boolean enabled;
    private long maxRequestSize;
    private int scanPageSize;
    private long updateScanInterval;
    private List<String> fileTypes = new LinkedList<>();
    private int objectLimit;
    private String assetGroupsUrl;
    private long scanJobPollInterval;
    private String version;
    private boolean scanResumptionEnabled;
    private int maximumFileSizeToScan;
    private int maximumFileSizeUpperLimit;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public long getMaxRequestSize() {
        return maxRequestSize;
    }

    /**
     * The maximum size of all the S3 objects in a single kafka message.
     */
    public void setMaxRequestSize(long maxRequestSize) {
        this.maxRequestSize = maxRequestSize;
    }

    public int getScanPageSize() {
        return scanPageSize;
    }

    /**
     * The maximum number of S3 object keys per list-objects call.
     */
    public void setScanPageSize(int scanPageSize) {
        this.scanPageSize = scanPageSize;
    }

    public long getUpdateScanInterval() {
        return updateScanInterval;
    }

    /**
     * Scan interval in millis
     */
    public void setUpdateScanInterval(long updateScanInterval) {
        this.updateScanInterval = updateScanInterval;
    }

    public List<String> getFileTypes() {
        return fileTypes;
    }

    /**
     * List of file types that can be scanned
     */
    public void setFileTypes(List<String> fileTypes) {
        this.fileTypes = fileTypes;
    }

    public int getObjectLimit() {
        return objectLimit;
    }

    /**
     * Maximum number of objects to include in a single scan request
     */
    public void setObjectLimit(int objectLimit) {
        this.objectLimit = objectLimit;
    }

    public String getAssetGroupsUrl() {
        return assetGroupsUrl;
    }

    /**
     * AssetGroups service URL for getting assets by group id
     */
    public void setAssetGroupsUrl(String assetGroupsUrl) {
        this.assetGroupsUrl = assetGroupsUrl;
    }

    public long getScanJobPollInterval() {
        return scanJobPollInterval;
    }

    /**
     * @param scanJobPollInterval Interval in milliseconds the service should look for new scan jobs to schedule
     */
    public void setScanJobPollInterval(long scanJobPollInterval) {
        this.scanJobPollInterval = scanJobPollInterval;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public boolean isScanResumptionEnabled() {
        return scanResumptionEnabled;
    }

    /**
     * @param scanResumptionEnabled Flag to indicate whether partially completed scans should be resumed
     */
    public void setScanResumptionEnabled(boolean scanResumptionEnabled) {
        this.scanResumptionEnabled = scanResumptionEnabled;
    }

    public int getMaximumFileSizeToScan() {
        return maximumFileSizeToScan;
    }

    /**
     * @param maximumFileSizeToScan Maximum size of a file that will be scanned by the S3 scanner (in GB)
     */
    public void setMaximumFileSizeToScan(int maximumFileSizeToScan) {
        this.maximumFileSizeToScan = maximumFileSizeToScan;
    }

    public int getMaximumFileSizeUpperLimit() {
        return maximumFileSizeUpperLimit;
    }

    /**
     * @param maximumFileSizeUpperLimit The OpenRaven-defined upper limit for a scan, customers cannot set maximum
     *                                  file size to be higher than this (in MB)
     */
    public void setMaximumFileSizeUpperLimit(int maximumFileSizeUpperLimit) {
        this.maximumFileSizeUpperLimit = maximumFileSizeUpperLimit;
    }

    private static final String ENABLED_NAME = "enabled";
    private static final String MAXREQUESTSIZE_NAME = "maxRequestSize";
    private static final String SCANPAGESIZE_NAME = "scanPageSize";
    private static final String UPDATESCANINTERVAL_NAME = "updateScanInterval";
    private static final String FILETYPES_NAME = "fileTypes";
    private static final String OBJECTLIMIT_NAME = "objectLimit";
    private static final String ASSETGROUPSURL_NAME = "assetGroupsUrl";
    private static final String SCANJOBPOLLINTERVAL_NAME = "scanJobPollInterval";
    private static final String VERSION_NAME = "version";
    private static final String SCANRESUMPTIONENABLED_NAME = "scanResumptionEnabled";
    private static final String MAXIMUMFILESIZETOSCAN_NAME = "maximumFileSizeToScan";
    private static final String MAXIMUMFILESIZEUPPERLIMIT_NAME = "maximumFileSizeUpperLimit";

    public void initialize(ZookeeperService zookeeperService) {
        this.enabled = Boolean.parseBoolean(zookeeperService.readFromZookeeper(ZK_PATH + "/" + ENABLED_NAME, "true"));
        this.maxRequestSize = Long.parseLong(zookeeperService.readFromZookeeper(ZK_PATH + "/" + MAXREQUESTSIZE_NAME, Long.toString(this.maxRequestSize)));
        this.scanPageSize = Integer.parseInt(zookeeperService.readFromZookeeper(ZK_PATH + "/" + SCANPAGESIZE_NAME, Integer.toString(this.scanPageSize)));
        this.updateScanInterval = Long.parseLong(zookeeperService.readFromZookeeper(ZK_PATH + "/" + UPDATESCANINTERVAL_NAME, Long.toString(this.updateScanInterval)));
        this.fileTypes = Optional.ofNullable(zookeeperService.readFromZookeeper(ZK_PATH + "/" + FILETYPES_NAME, null)).map(str -> Arrays.asList(str.split("\\s*,\\s*"))).orElse(this.fileTypes);
        this.objectLimit = Integer.parseInt(zookeeperService.readFromZookeeper(ZK_PATH + "/" + OBJECTLIMIT_NAME, Integer.toString(this.objectLimit)));
        this.assetGroupsUrl = zookeeperService.readFromZookeeper(ZK_PATH + "/" + ASSETGROUPSURL_NAME, this.assetGroupsUrl);
        this.scanJobPollInterval = Long.parseLong(zookeeperService.readFromZookeeper(ZK_PATH + "/" + SCANJOBPOLLINTERVAL_NAME, Long.toString(this.scanJobPollInterval)));
        this.version = zookeeperService.readFromZookeeper(ZK_PATH + "/" + VERSION_NAME, this.version);
        this.scanResumptionEnabled = Boolean.parseBoolean(zookeeperService.readFromZookeeper(ZK_PATH + "/" + SCANRESUMPTIONENABLED_NAME, "true"));
        this.maximumFileSizeToScan = Integer.parseInt(zookeeperService.readFromZookeeper(ZK_PATH + "/" + MAXIMUMFILESIZETOSCAN_NAME, Integer.toString(this.maximumFileSizeToScan)));
        this.maximumFileSizeUpperLimit = Integer.parseInt(zookeeperService.readFromZookeeper(ZK_PATH + "/" + MAXIMUMFILESIZEUPPERLIMIT_NAME, Integer.toString(this.maximumFileSizeUpperLimit)));
    }
}
