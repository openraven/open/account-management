package io.openraven.properties.zookeeper;

import org.springframework.boot.context.properties.ConfigurationProperties;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Is populated from Zookeeper and has a watch that updates fields on key change.
 * This object also is serialized out the end user in Odin as part of server settings.
 */
@ConfigurationProperties(CloudIngestionProperties.CONFIG_PROP)
public class CloudIngestionProperties {
    public static final String CONFIG_PROP = "openraven.app.v1.cloud-ingestion";
    public static final String ZK_PATH = CONFIG_PROP.replaceAll("\\.", "/");

    private Analytics analytics = new Analytics();

    public static class Analytics {
        String clusterId;

        public boolean isEnabled() {
            return isNotBlank(clusterId);
        }

        public String getClusterId() {
            return clusterId;
        }

        public void setClusterId(String clusterId) {
            this.clusterId = clusterId;
        }
    }

    public Analytics getAnalytics() {
        return analytics;
    }

    public void setAnalytics(Analytics analytics) {
        this.analytics = analytics;
    }

}
