package io.openraven.properties.zookeeper.models;


import com.fasterxml.jackson.annotation.JsonCreator;
import io.openraven.services.ZookeeperService;


public class SchedulerPropertiesModel {

    public static final String DATA_SCAN_CONFIG_PROP = "openraven.app.v1.s3.scheduling";
    public static final String ZK_PATH = DATA_SCAN_CONFIG_PROP.replaceAll("\\.", "/");


    private Integer maxConcurrent = 5;
    private boolean enabled = true;
    private int buckets;
    private int files;

    @JsonCreator
    public SchedulerPropertiesModel() {

    }

    public int getMaxConcurrent() {
        return maxConcurrent;
    }

    /**
     * @param maxConcurrent maximum concurrent number of Lambdas to be schedules for this type of request
     */
    public void setMaxConcurrent(int maxConcurrent) {
        this.maxConcurrent = maxConcurrent;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getBuckets() {
        return buckets;
    }

    /**
     * Set the maximum number of buckets in S3 that are scanned at once. A given scanner will process scan requests
     * until completion of the full bucket.
     *
     * @param buckets - The number of buckets to concurrently scan
     */
    public void setBuckets(int buckets) {
        this.buckets = buckets;
    }

    public int getFiles() {
        return files;
    }

    /**
     * Set the maximum number of files to scan in an s3 bucket. This allows setting a maximum depth of files to ensure
     * scans will complete.
     *
     * @param files - The number of files to scan
     */
    public void setFiles(int files) {
        this.files = files;
    }
    private static final String MAXCONCURRENT_NAME = "maxConcurrent";
    private static final String ENABLED_NAME = "enabled";
    private static final String BUCKETS_NAME = "buckets";
    private static final String FILES_NAME = "files";
    public void initialize(ZookeeperService zookeeperService) {
        this.maxConcurrent = Integer.parseInt(zookeeperService.readFromZookeeper(ZK_PATH + "/" + MAXCONCURRENT_NAME, Integer.toString(this.maxConcurrent)));
        this.enabled = Boolean.valueOf(zookeeperService.readFromZookeeper(ZK_PATH + "/" + ENABLED_NAME, "true"));
        this.buckets = Integer.parseInt(zookeeperService.readFromZookeeper(ZK_PATH + "/" + BUCKETS_NAME, Integer.toString(this.buckets)));
        this.files = Integer.parseInt(zookeeperService.readFromZookeeper(ZK_PATH + "/" + FILES_NAME, Long.toString(this.files)));
    }
}
