package io.openraven.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("openraven.app.v1.cluster")
public class ClusterProperties {
    private String name;
    private String asgardUrl;
    private String parent;
    private String separator;
    private String releaseChannel;
    private String releaseVersion;

    public String getReleaseChannel() {
        return releaseChannel;
    }

    public void setReleaseChannel(String releaseChannel) {
        this.releaseChannel = releaseChannel;
    }

    public String getReleaseVersion() {
        return releaseVersion;
    }

    public void setReleaseVersion(String releaseVersion) {
        this.releaseVersion = releaseVersion;
    }

    private ClusterProperties(){}

    public String getName() {
        return name;
    }

    /**
     * The name of the organization selected when the software was installed
     */
    public void setName(String name) {
        this.name = name;
    }

    public String getAsgardUrl() {
        return this.asgardUrl;
    }

    public void setAsgardUrl(String asgardUrl) {
        this.asgardUrl = asgardUrl;
    }

    public String getParent() {
        return this.parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getSeparator() {
        return this.separator;
    }

    public void setSeparator(String separator) {
        this.separator = separator;
    }

}
