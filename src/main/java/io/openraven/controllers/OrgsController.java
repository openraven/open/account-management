package io.openraven.controllers;

import io.openraven.models.AnalyticsInfo;
import io.openraven.models.OrganizationParticipation;
import io.openraven.models.OrganizationSummary;
import io.openraven.models.User;
import io.openraven.properties.ClusterProperties;
import io.openraven.properties.zookeeper.CloudIngestionProperties;
import io.openraven.services.OrgsService;
import io.openraven.services.ZookeeperService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;

@RequestMapping("api/orgs")
@RestController
public class OrgsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrgsController.class);
    private final OrgsService orgsService;
    private final ClusterProperties clusterProperties;
    private final ZookeeperService zookeeperService;
    private final String clusterId;
    private final AnalyticsInfo analyticsInfo;

    public OrgsController(OrgsService orgsService, ClusterProperties clusterProperties, ZookeeperService zookeeperService, CloudIngestionProperties cloudIngestionProperties) {
        this.orgsService = orgsService;
        this.clusterProperties = clusterProperties;
        this.zookeeperService = zookeeperService;
        this.clusterId = cloudIngestionProperties.getAnalytics().getClusterId();
        this.analyticsInfo = new AnalyticsInfo(clusterId, clusterProperties);
    }

    @PreAuthorize("hasRole('user')")
    @GetMapping("/info")
    public ResponseEntity<OrganizationSummary> info(Principal user) {
        LOGGER.info("called API 'info' with user={}", user.toString());
        LOGGER.info("User {}", user);
        OrganizationSummary organizationSummary = this.orgsService.getOrgSummary(clusterProperties.getName(), user);
        LOGGER.info("Organization Summary {}", organizationSummary);
        organizationSummary.numRoleArns = zookeeperService.getRoleArnsSize();
        organizationSummary.analyticsId = clusterId;
        organizationSummary.channel = clusterProperties.getReleaseChannel();
        organizationSummary.version = clusterProperties.getReleaseVersion();
        return ResponseEntity.ok(organizationSummary);
    }

    @GetMapping("/info/analytics")
    public ResponseEntity<AnalyticsInfo> analyticsInfo() {
        return ResponseEntity.ok(analyticsInfo);
    }

    @PreAuthorize("hasRole('user')")
    @GetMapping("/info/users")
    public ResponseEntity<List<User>> infoUsers(Principal user) {
        LOGGER.info("called API 'infoUsers' with user={}", user.toString());
        var response = this.orgsService.listUsers(clusterProperties.getName(), user);
        return ResponseEntity.ok(Arrays.asList(response));
    }


    @PreAuthorize("hasRole('user')")
    @GetMapping("/info/me")
    public ResponseEntity<User> infoMe(Principal user) {
        LOGGER.info("called API 'infoMe' with user={}", user.toString());
        var response = this.orgsService.fetchUser(user);
        return ResponseEntity.ok(response);
    }

    @PreAuthorize("hasRole('admin')")
    @GetMapping("/{orgName}")
    public ResponseEntity<OrganizationSummary> admin(Principal user, @PathVariable("orgName") String orgName) {
        LOGGER.info("called API 'admin' with user={}, orgName={}", user.toString(), orgName);
        var body = this.orgsService.getOrgSummary(orgName, user);
        body.analyticsId = this.clusterId;
        return ResponseEntity.ok(body);
    }

    @PreAuthorize("hasRole('owner')")
    @PostMapping("/{orgName}/role/owner/{targetUserName}")
    public ResponseEntity<String> promoteOwner(Principal currentOwner, @PathVariable("targetUserName") String targetUserName, @PathVariable("orgName") String orgName) {
        LOGGER.info("called API 'promoteOwner' with currentOwner={}, targetUserName={}, orgName={}", currentOwner.toString(), targetUserName, orgName);
        var responseBody = this.orgsService.promoteOwner(orgName, currentOwner, targetUserName);
        return ResponseEntity.ok(responseBody);
    }

    @PreAuthorize("hasRole('owner') or hasRole('admin')")
    @PostMapping("/{orgName}/role/admin/{targetUserName}")
    public ResponseEntity<String> promoteAdmin(Principal currentUser, @PathVariable("targetUserName") String targetUserName, @PathVariable("orgName") String orgName) {
        LOGGER.info("called API 'promoteAdmin' with currentUser={}, targetUserName={}, orgName={}", currentUser.toString(), targetUserName, orgName);
        var responseBody = this.orgsService.promoteAdmin(orgName, currentUser, targetUserName);
        return ResponseEntity.ok(responseBody);
    }

    @PreAuthorize("hasRole('owner') or hasRole('admin')")
    @DeleteMapping("/{orgName}/role/admin/{targetUserName}")
    public ResponseEntity<String> demoteAdmin(Principal currentUser, @PathVariable("targetUserName") String targetUserName, @PathVariable("orgName") String orgName) {
        LOGGER.info("called API 'demoteAdmin' with currentUser={}, targetUserName={}, orgName={}", currentUser.toString(), targetUserName, orgName);
        var response = this.orgsService.demoteAdmin(orgName, currentUser, targetUserName);
        return ResponseEntity.ok(response);
    }

    @PreAuthorize("hasRole('user')")
    @GetMapping("/")
    public List<OrganizationParticipation> anyone(Authentication user) {
        LOGGER.info("called API 'anyone' with user={}", user.toString());
        return this.orgsService.listOrgs(user);
    }


    @PreAuthorize("hasRole('owner')")
    @DeleteMapping("/{orgName}")
    public ResponseEntity<String> deleteOrg(@AuthenticationPrincipal Principal user, @PathVariable("orgName") String orgName) {
        LOGGER.info("called API 'deleteOrg' with user={}, orgName={}", user.toString(), orgName);
        var response = this.orgsService.deleteOrg(orgName, user);
        return ResponseEntity.ok(response);
    }

    @PreAuthorize("hasRole('user')")
    @PostMapping("/{orgName}/invite/{email}")
    public ResponseEntity<String> inviteUser(Principal user, @PathVariable("orgName") String orgName, @PathVariable("email") String targetEmail) {
        LOGGER.info("called API 'inviteUser' with user={}, orgName={}, targetEmail={}", user.toString(), orgName, targetEmail);
        var response = this.orgsService.invite(orgName, targetEmail, user);
        return ResponseEntity.ok(response);
    }

    @PreAuthorize("hasRole('user')")
    @DeleteMapping("/{orgName}/invite/{email}")
    public ResponseEntity<String> deleteInvite(Principal user, @PathVariable("orgName") String orgName, @PathVariable("email") String targetEmail) {
        LOGGER.info("called API 'deleteInvite' with user={}, orgName={}, targetEmail={}", user.toString(), orgName, targetEmail);
        var response = this.orgsService.deleteInvite(orgName, targetEmail, user);
        return ResponseEntity.ok(response);
    }

    @PreAuthorize("hasRole('owner') or hasRole('admin')")
    @DeleteMapping("/{orgName}/users/{targetUserName}")
    public ResponseEntity<String> removeUser(Principal user, @PathVariable("orgName") String orgName, @PathVariable("targetUserName") String targetEmail) {
        LOGGER.info("called API 'removeUser' with user={}, orgName={}, targetEmail={}", user.toString(), orgName, targetEmail);
        var response = this.orgsService.removeUser(orgName, targetEmail, user);
        return ResponseEntity.ok(response);
    }

    @PreAuthorize("hasRole('user')")
    @GetMapping("/{orgName}/users")
    public ResponseEntity<List<User>> findUsers(Principal user, @PathVariable("orgName") String orgName) {
        LOGGER.info("called API 'findUsers' with user={}, orgName={}", user.toString(), orgName);
        User[] response = this.orgsService.listUsers(orgName, user);
        return ResponseEntity.ok(Arrays.asList(response));
    }
}
