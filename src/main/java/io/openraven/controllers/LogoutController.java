package io.openraven.controllers;

import io.openraven.eventlog.EventLogger;
import io.openraven.eventlog.shared.Event;
import io.openraven.properties.ClusterProperties;
import io.sentry.Sentry;
import io.sentry.SentryLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Map;

import static java.lang.String.format;

@RequestMapping("api/logout")
@RestController
public class LogoutController {

    private final WebClient webClient;
    private final ClusterProperties clusterProperties;
    private final EventLogger eventLogger;

    private static final Logger LOGGER = LoggerFactory.getLogger(LogoutController.class);

    public LogoutController(WebClient webClient, ClusterProperties clusterProperties, EventLogger eventLogger) {
        this.webClient = webClient;
        this.clusterProperties = clusterProperties;
        this.eventLogger = eventLogger;
    }

    @PostMapping
    public ResponseEntity<Void> logout(Authentication user) {
        LOGGER.info("called API 'logout' with user={}", user.getName());
        final ResponseEntity<Void> redirectResponseEntity = this.webClient.post().uri(createLogoutUri(user)).retrieve().toBodilessEntity().block();

        int result;
        if (redirectResponseEntity != null) {
            result = redirectResponseEntity.getStatusCodeValue();
        } else {
            result = 0;
        }
        if (result != HttpStatus.FOUND.value()) {
            Sentry.withScope(scope -> {
                scope.setLevel(SentryLevel.ERROR);
                scope.setContexts("additional data", Map.of(
                        "httpstatus", result,
                        "user", user));
                Sentry.captureMessage("Failed to log out user due to non-200 status");
            });
        }
        else{
            var eventData = new Event.EventData();
            eventData.put("USER", user.getName());
            String eventMessage = "User logout";
            Event event = new Event.Builder()
                    .eventCategory("AUTH")
                    .eventType("USER_LOGOUT")
                    .message(eventMessage)
                    .eventData(eventData)
                    .build();
            eventLogger.logWithRemoteUser(event);
        }

        return redirectResponseEntity;
    }

    private String createLogoutUri(Authentication user) {
        return format(
                "%s/api/user/sessions/logout/%s/%s",
                clusterProperties.getAsgardUrl(),
                clusterProperties.getName(),
                user.getName());
    }
}
