package io.openraven.controllers;

import io.openraven.models.DataScanSettings;
import io.openraven.services.ZookeeperService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/scan/settings")
public class ScanSettingsController {

  private final ZookeeperService zookeeperService;

  private static final Logger LOGGER = LoggerFactory.getLogger(ScanSettingsController.class);

  public ScanSettingsController(ZookeeperService zookeeperService) {
    this.zookeeperService = zookeeperService;
  }

  @Deprecated
  @GetMapping
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void getScanSettings() {
  }

  @Deprecated
  @PostMapping("dmapscanningenabled")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void setDmapScanningEnabled(@RequestParam boolean ignored) {
  }

  @Deprecated
  @PostMapping("scanquotaperregion")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void setScanQuotaPerRegion(@RequestParam int ignored) {
  }

  @Deprecated
  @PostMapping("scaninterval")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void setScanInterval(@RequestParam int ignored) {
  }

  @GetMapping("datascanning")
  public DataScanSettings getDataScanSettings() {
    LOGGER.info("called API 'getDataScanSettings'");
    return this.zookeeperService.getDataScanSettings();
  }

  @PostMapping(value = "datascanning", consumes = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void setDataScanningSettings(@RequestBody DataScanSettings dataScanningSettings) {
    LOGGER.info("called API 'setDataScanningSettings' with dataScanningSettings={}", dataScanningSettings.toString());
    this.zookeeperService.setDataScanSettings(dataScanningSettings);
  }

}
