package io.openraven.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * This class had several unused in productui apis on it. They have been removed, leaving only deprecated methods.
 */
@Deprecated
@RestController
@RequestMapping("/api/server/settings")
public class ServerSettingsController {


    private static final Logger LOGGER = LoggerFactory.getLogger(ServerSettingsController.class);
    private static final Pattern LETTERS_DASHES_AND_COMMAS = Pattern.compile("[a-zA-Z0-9\\-]+");

    private boolean validateRegions(String[] regions) {
        LOGGER.info("called API 'validateRegions' with regions={}", String.join(",", regions));
        return Stream.of(regions).allMatch((string) -> LETTERS_DASHES_AND_COMMAS.matcher(string).matches());
    }

    /**
     * Legacy endpoint deprecated for removal. Was used in productui to indicate if a cluster was connected to an
     * aws account. This would either show an empty map (if false and no data) or FIUE (if true).
     * <p>
     * GetMapping now always returns false
     * </p>
     *
     * @return OK with body false
     */
    @Deprecated
    @GetMapping("demodb")
    public ResponseEntity<Boolean> getDemoDbEnablement() {
        LOGGER.info("called API 'getDemoDbEnablement'");
        return new ResponseEntity<>(false, HttpStatus.OK);
    }

    /**
     * Legacy endpoint deprecated for removal. Was used in productui to indicate if a cluster was connected to an
     * aws account. This would either show an empty map (if false and no data) or FIUE (if true).
     * <p>
     * Has no effect on server state
     * </p>
     *
     * @return NO_CONTENT
     */
    @Deprecated
    @DeleteMapping("demodb")
    public ResponseEntity<Void> setDemoDbEnablement() {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * Legacy endpoint deprecated for removal. Was used in productui to indicate if a cluster was connected to an
     * aws account. This would either show an empty map (if false and no data) or FIUE (if true).
     * <p>
     * Has no effect on server state
     * </p>
     *
     * @return NO_CONTENT
     */
    @Deprecated
    @PostMapping("demodb")
    public ResponseEntity<Void> setDemoDbOn(@RequestBody String status) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
