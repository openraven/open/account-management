package io.openraven.controllers;

import io.openraven.models.GoogleWorkspaceConfigSettings;
import io.openraven.models.ProjectLocalServiceAccountRequest;
import io.openraven.services.google.GcpProjectService;
import io.openraven.services.google.GoogleWorkspaceSettingsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/gcp")
public class GcpController {
    private final GcpProjectService gcpProjectService;
    private final GoogleWorkspaceSettingsService googleWorkspaceSettingsService;

    public GcpController(GcpProjectService gcpProjectService,
                         GoogleWorkspaceSettingsService googleWorkspaceSettingsService) {
        this.gcpProjectService = gcpProjectService;
        this.googleWorkspaceSettingsService = googleWorkspaceSettingsService;
    }

    @PostMapping("/projectLocalServiceAccount")
    public void configureScanningServiceAccount(@RequestBody ProjectLocalServiceAccountRequest request) {
        gcpProjectService.connectProject(request.getProjectId(), request.getServiceAccountEmail());
    }

    @PostMapping("/google-workspace/super-admin-email")
    public String setGoogleWorkspaceSuperAdmin(@RequestBody String email) {
        return gcpProjectService.setGoogleWorkspaceSuperAdmin(email);
    }

    @GetMapping("/google-workspace/super-admin-email")
    public String getGoogleWorkspaceSuperAdmin() {
        return gcpProjectService.getGoogleWorkspaceSuperAdmin();
    }

    @GetMapping("/google-workspace/settings")
    public ResponseEntity<GoogleWorkspaceConfigSettings> getGoogleWorkspaceSettings() {
        return ResponseEntity.ok(googleWorkspaceSettingsService.fetch());
    }

    @PutMapping("/google-workspace/settings")
    public ResponseEntity<Void> saveGoogleWorkspaceSettings(@RequestBody GoogleWorkspaceConfigSettings settings) {
        googleWorkspaceSettingsService.saveAll(settings);
        return ResponseEntity.ok().build();
    }
}
