package io.openraven.models;

public class User {
    public String firstName;
    public String lastName;
    public String email;

    /* custom fields */

    public String birthday;
    public String twitter;
    public String linkedIn;
    public String profileUrl;
    public boolean companyNewsletterOptIn;
    public boolean productNewsletterOptIn;
    public boolean blogPostsOptIn;
    public boolean communitySlackOptIn;
    public boolean devSlackOptIn;
    public long createdDate;
    public String role;
    public String status;

    public String profileAvatarUrl;
}
