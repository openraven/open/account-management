package io.openraven.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ProjectLocalServiceAccountRequest {
    private final String projectId;
    private final String serviceAccountEmail;

    @JsonCreator
    public ProjectLocalServiceAccountRequest(@JsonProperty("projectId") String projectId,
                                             @JsonProperty("serviceAccountEmail") String serviceAccountEmail) {
        this.projectId = projectId;
        this.serviceAccountEmail = serviceAccountEmail;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getServiceAccountEmail() {
        return serviceAccountEmail;
    }
}
