package io.openraven.models;

import java.util.Date;

public class OrganizationSummary {
    @SuppressWarnings(value = "unused")
    public String currentUserRole;
    public int numUsers;
    public int numAdmins;
    @SuppressWarnings(value = "unused")
    public String edition = "Community Edition";
    public String channel = "openraven-deploy";
    public String version;

    public String ownerFirstName;
    public String ownerLastName;
    public Date creationDate;
    @SuppressWarnings("unused")
    public String status = "Active";
    public String name;
    public String clusterUrl;
    public String analyticsId;

    public int numRoleArns;

    @Override
    public String toString() {
        return "OrganizationSummary{" +
                "edition='" + edition + '\'' +
                ", currentUserRole='" + currentUserRole + '\'' +
                ", numUsers=" + numUsers +
                ", numAdmins=" + numAdmins +
                ", channel='" + channel + '\'' +
                ", ownerFirstName='" + ownerFirstName + '\'' +
                ", ownerLastName='" + ownerLastName + '\'' +
                ", creationDate=" + creationDate +
                ", status='" + status + '\'' +
                ", name='" + name + '\'' +
                ", clusterUrl='" + clusterUrl + '\'' +
                ", numRoleArns=" + numRoleArns +
                '}';
    }
}
