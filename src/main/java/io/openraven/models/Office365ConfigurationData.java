package io.openraven.models;

public class Office365ConfigurationData {
    public String clientId;
    public String clientSecret;
    public String tenantAuthority;
    public String defaultScope;
    public int lookbackDays;
    public boolean enabled;

    @Override
    public String toString(){
        return String.format("{clientId=%s, clientSecret=REDACTED, tenantAuthority=%s, defaultScope=%s, lookbackDay=%s, enabled=%s}",
                clientId, tenantAuthority, defaultScope, lookbackDays, enabled);
    }
}
