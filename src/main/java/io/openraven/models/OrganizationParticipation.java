package io.openraven.models;

public class OrganizationParticipation {
    public String orgName;
    public String role;
    public String orgType = "Community Edition";
    public String status = "ACTIVE";
}
