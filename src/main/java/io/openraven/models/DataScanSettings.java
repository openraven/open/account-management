package io.openraven.models;

import io.openraven.properties.zookeeper.ScannerProperties;
import io.openraven.properties.zookeeper.models.SchedulerPropertiesModel;

public class DataScanSettings {
    public boolean s3Activation;
    public boolean activation;
    public int buckets;
    public int files;
    public int maximumFileSizeToScan;
    public int allowedMaximumFileSize;

    public DataScanSettings(SchedulerPropertiesModel schedulerPropertiesModel, ScannerProperties scannerProperties) {
        this.activation = schedulerPropertiesModel.isEnabled();
        this.buckets = schedulerPropertiesModel.getBuckets();
        this.files = schedulerPropertiesModel.getFiles();
        this.maximumFileSizeToScan = scannerProperties.getMaximumFileSizeToScan();
        this.s3Activation = scannerProperties.isEnabled();
        this.allowedMaximumFileSize = scannerProperties.getMaximumFileSizeUpperLimit();
    }

    public DataScanSettings() {

    }

    @Override
    public String toString() {
        return String.format("{s3Activation=%s, activation=%s, buckets=%s, files=%s, maximumFileSizeToScan=%s, allowedMaximumFileSize=%s}",
                s3Activation, activation, buckets, files, maximumFileSizeToScan, allowedMaximumFileSize);
    }
}
