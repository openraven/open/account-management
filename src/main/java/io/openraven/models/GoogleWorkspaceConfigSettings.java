package io.openraven.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.UUID;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GoogleWorkspaceConfigSettings {

    private final String superUserEmail;
    private final ScanSource scanSource;
    private final String awsAccountId;
    private final String awsRegion;
    private final String gcpRegion;
    private final Long scanBudget;
    private final List<UUID> autoScanDataCollections;
    private final String quarantineText;
    private final List<String> allowedDomains;
    private final List<String> deniedDomains;
    private final boolean externalSharingScanEnabled;

    @JsonCreator
    public GoogleWorkspaceConfigSettings(@JsonProperty String superUserEmail,
                                         @JsonProperty ScanSource scanSource,
                                         @JsonProperty String awsAccountId,
                                         @JsonProperty String awsRegion,
                                         @JsonProperty String gcpRegion,
                                         @JsonProperty Long scanBudget,
                                         @JsonProperty List<UUID> autoScanDataCollections,
                                         @JsonProperty String quarantineText,
                                         @JsonProperty List<String> allowedDomains,
                                         @JsonProperty List<String> deniedDomains,
                                         @JsonProperty boolean externalSharingScanEnabled) {
        this.superUserEmail = superUserEmail;
        this.scanSource = scanSource;
        this.awsAccountId = awsAccountId;
        this.awsRegion = awsRegion;
        this.gcpRegion = gcpRegion;
        this.scanBudget = scanBudget;
        this.autoScanDataCollections = autoScanDataCollections;
        this.quarantineText = quarantineText;
        this.allowedDomains = allowedDomains;
        this.deniedDomains = deniedDomains;
        this.externalSharingScanEnabled = externalSharingScanEnabled;
    }

    public String getSuperUserEmail() {
        return superUserEmail;
    }

    public ScanSource getScanSource() {
        return scanSource;
    }

    public String getAwsAccountId() {
        return awsAccountId;
    }

    public String getAwsRegion() {
        return awsRegion;
    }

    public String getGcpRegion() {
        return gcpRegion;
    }

    public Long getScanBudget() {
        return scanBudget;
    }

    public List<UUID> getAutoScanDataCollections() {
        return autoScanDataCollections;
    }

    public String getQuarantineText() {
        return quarantineText;
    }

    public List<String> getAllowedDomains() {
        return allowedDomains;
    }

    public List<String> getDeniedDomains() {
        return deniedDomains;
    }

    public boolean isExternalSharingScanEnabled() {
        return externalSharingScanEnabled;
    }
}
