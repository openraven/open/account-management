package io.openraven.models;

import io.openraven.properties.ClusterProperties;

// Class to hold and initialize common values to send on Segment Analytics events
public class AnalyticsInfo {
    private static final String WORK_SPACE_ID_FORMAT = "%s:%s";
    public final String clusterId;
    public final String clusterName;
    public final String workspaceId;
    public final String releaseChannel;
    public final String releaseVersion;

    public AnalyticsInfo(String clusterId, ClusterProperties clusterProperties) {
        this.clusterName = clusterProperties.getName();
        this.clusterId = clusterId;
        this.workspaceId = String.format(WORK_SPACE_ID_FORMAT, clusterName, clusterId);
        this.releaseChannel = clusterProperties.getReleaseChannel();
        this.releaseVersion = clusterProperties.getReleaseVersion();
    }
}
