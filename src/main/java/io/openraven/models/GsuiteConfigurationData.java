package io.openraven.models;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;

import static java.io.File.createTempFile;

public class GsuiteConfigurationData {
    public String serviceAccountEmail;
    public String directoryEmail;
    public int lookbackDays;
    public boolean enabled;
    public String serviceAccountKeyFileContent;


    public File create12TempFile() {
        final byte[] decode = Base64.getDecoder().decode(serviceAccountKeyFileContent);
        try {
            File file = createTempFile("orvn", "p12");
            try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
                fileOutputStream.write(decode);
                fileOutputStream.flush();
            }
            return file;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString(){
        return String.format("{serviceAccountEmail=%s, directoryEmail=%s, serviceAccountKeyFileContent=REDACTED, lookbackDay=%s, enabled=%s}",
                serviceAccountEmail, directoryEmail, lookbackDays, enabled);
    }
}
