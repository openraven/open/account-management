package io.openraven.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ConfigureServiceAccountImpersonationRequest {
    private final String groupId;
    private final String projectLocalServiceAccountEmail;

    @JsonCreator
    public ConfigureServiceAccountImpersonationRequest(@JsonProperty("groupId") String groupId,
                                                       @JsonProperty("projectLocalServiceAccountEmail") String projectLocalServiceAccountEmail) {
        this.groupId = groupId;
        this.projectLocalServiceAccountEmail = projectLocalServiceAccountEmail;
    }

    @JsonProperty("groupId")
    public String getGroupId() {
        return groupId;
    }

    @JsonProperty("projectLocalServiceAccountEmail")
    public String getProjectLocalServiceAccountEmail() {
        return projectLocalServiceAccountEmail;
    }
}
