package io.openraven;

import io.openraven.authoritymapper.OpenravenGrantedAuthoritiesMapper;
import io.openraven.properties.ClusterProperties;
import io.openraven.properties.zookeeper.CloudIngestionProperties;
import io.openraven.properties.zookeeper.ScannerProperties;
import org.springframework.boot.Banner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.zookeeper.config.ZookeeperConfigProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.client.AuthorizedClientServiceOAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientProvider;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientProviderBuilder;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.reactive.function.client.ServletOAuth2AuthorizedClientExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

@SpringBootApplication
@EnableJpaRepositories("io.openraven")
@EntityScan(basePackages = {"io.openraven"})
public class App {
    public static void main(String[] args) {
        new SpringApplicationBuilder(App.class)
                .bannerMode(Banner.Mode.OFF)
                .web(WebApplicationType.SERVLET)
                .run(args);
    }

    @EnableScheduling
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    @Configuration
    @EnableConfigurationProperties(
            {
                    ClusterProperties.class,
                    CloudIngestionProperties.class,
                    ScannerProperties.class,
                    ZookeeperConfigProperties.class,
            }
    )
    public static class Config {
        /**
         * Be sure this name lines up with the <tt>{ client: { registration:</tt> key in <tt>application.yml</tt>.
         */
        private static final String CLIENT_REGISTRATION_ID = "asgard";


        @Bean
        public OAuth2AuthorizedClientManager authorizedClientManager(
                ClientRegistrationRepository clientRegistrationRepository,
                OAuth2AuthorizedClientService authorizedClientRepository) {

            OAuth2AuthorizedClientProvider authorizedClientProvider =
                    OAuth2AuthorizedClientProviderBuilder.builder()
                            .clientCredentials()
                            .build();

            AuthorizedClientServiceOAuth2AuthorizedClientManager authorizedClientManager =
                    new AuthorizedClientServiceOAuth2AuthorizedClientManager(
                            clientRegistrationRepository, authorizedClientRepository);
            authorizedClientManager.setAuthorizedClientProvider(authorizedClientProvider);

            return authorizedClientManager;
        }

        @Bean
        @Profile("!integration-test")
        WebClient webClient(OAuth2AuthorizedClientManager authorizedClientManager) {
            ServletOAuth2AuthorizedClientExchangeFilterFunction oauth2Client =
                    new ServletOAuth2AuthorizedClientExchangeFilterFunction(authorizedClientManager);
            oauth2Client.setDefaultClientRegistrationId(CLIENT_REGISTRATION_ID);
            return WebClient.builder()
                    .clientConnector(new ReactorClientHttpConnector(HttpClient.newConnection()))
                    .apply(oauth2Client.oauth2Configuration())
                    .build();
        }

        @Bean
        @Profile("!integration-test")
        WebClient internalWebClient() {
            return WebClient.builder().clientConnector(new ReactorClientHttpConnector(HttpClient.newConnection())).build();
        }
    }

    @Configuration
    @Profile("!integration-test")
    static class OpenravenOAuth2WebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

        public OpenravenOAuth2WebSecurityConfigurerAdapter(OpenravenGrantedAuthoritiesMapper openravenGrantedAuthoritiesMapper) {
            super();
            this.openravenGrantedAuthoritiesMapper = openravenGrantedAuthoritiesMapper;
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .oauth2ResourceServer().jwt()
                    .jwtAuthenticationConverter(openravenGrantedAuthoritiesMapper)
                    .and()
                    .and()
                    .oauth2Login().userInfoEndpoint().userAuthoritiesMapper(openravenGrantedAuthoritiesMapper)
                    .and()
                    .and()
                    .authorizeRequests()
                    .antMatchers("**/demodb", "**/scan/settings", "**/cloudwatcher/settings/**").permitAll()
                    .and()
                    .csrf().disable()
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER)
            ;
        }

        private final OpenravenGrantedAuthoritiesMapper openravenGrantedAuthoritiesMapper;

    }
}
