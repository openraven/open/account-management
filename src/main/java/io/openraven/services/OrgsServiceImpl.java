package io.openraven.services;

import io.openraven.eventlog.EventLogger;
import io.openraven.eventlog.shared.Event;
import io.openraven.models.OrganizationParticipation;
import io.openraven.models.OrganizationSummary;
import io.openraven.models.User;
import io.openraven.properties.ClusterProperties;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.security.Principal;
import java.util.List;

import static java.lang.String.format;
import static java.lang.String.join;
import static java.util.stream.Collectors.toList;

@Component
public class OrgsServiceImpl implements OrgsService {
    private static final String USER = "USER";

    private final ClusterProperties clusterProperties;
    private final WebClient webClient;
    private final EventLogger eventLogger;

    public OrgsServiceImpl(ClusterProperties clusterProperties, WebClient webClient, EventLogger eventLogger) {
        this.clusterProperties = clusterProperties;
        this.webClient = webClient;
        this.eventLogger = eventLogger;
    }

    @Override
    public List<OrganizationParticipation> listOrgs(Authentication user) {
        return user.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .filter(this::filterAuthority)
                .map(this::stripBaseName)
                .map(this::translateStrippedGroupNameToOrganizationParticipation)
                .collect(toList());
    }

    private String stripBaseName(String baseGroupName) {
        String joinedStrings = join(
                "",
                clusterProperties.getSeparator(),
                clusterProperties.getParent(),
                clusterProperties.getSeparator()
        );
        return baseGroupName.substring(
                baseGroupName.indexOf(
                        joinedStrings
                ) + joinedStrings.length()
        );
    }

    private boolean filterAuthority(String grantedAuthority) {
        return grantedAuthority.startsWith(clusterProperties.getSeparator());
    }

    private OrganizationParticipation translateStrippedGroupNameToOrganizationParticipation(String strippedGroupName) {
        OrganizationParticipation retVal = new OrganizationParticipation();
        String[] groupNameAndRole = strippedGroupName.split(clusterProperties.getSeparator());

        if (groupNameAndRole.length == 1) {
            retVal.role = USER;
            retVal.orgName = strippedGroupName;
        } else {
            String right = groupNameAndRole[1];
            String left = groupNameAndRole[0];
            retVal.orgName = left;
            String roleName = right.substring(right.indexOf(left + "-") + left.length() + 1);
            if (roleName.equals("owners")) {
                retVal.role = "OWNER";
            } else if (roleName.equals("admins")) {
                retVal.role = "ADMIN";
            }
        }
        return retVal;
    }

    @Override
    public String deleteOrg(String orgName, Principal user) {
        String url = createDeleteOrgUrl(orgName);
        return this.webClient.method(HttpMethod.DELETE).uri(url).body(Mono.just(user.getName()), String.class).accept(MediaType.TEXT_PLAIN).retrieve().bodyToMono(String.class).block();
    }

    @Override
    public User[] listUsers(String orgName, Principal user) {
        String url = createListUsersUrl(orgName);
        return this.webClient.post().uri(url).body(Mono.just(user.getName()), String.class).accept(MediaType.APPLICATION_JSON).retrieve().bodyToMono(User[].class).block();
    }

    @Override
    public User fetchUser(Principal user) {
        String name = user.getName();
        String fetchUserUrl = createFetchUserReportUrl(name);
        return this.webClient.post().uri(fetchUserUrl).body(Mono.just(name), String.class).accept(MediaType.APPLICATION_JSON).retrieve().bodyToMono(User.class).block();
    }

    private String createFetchUserReportUrl(String userName) {
        return format("%s/api/accounts/user/profile/%s/report", clusterProperties.getAsgardUrl(), userName);
    }

    private String createListUsersUrl(String orgName) {
        return format("%s/api/groups/%s/users", clusterProperties.getAsgardUrl(), orgName);
    }

    private String createDeleteOrgUrl(String orgName) {
        return format("%s/api/groups/%s", clusterProperties.getAsgardUrl(), orgName);
    }


    @Override
    public String promoteOwner(String orgName, Principal currentOwner, String targetUserName) {
        String promoteOwnerUrl = createPromoteUrl(orgName, targetUserName, "owner");
        return this.webClient.post().uri(promoteOwnerUrl).body(Mono.just(currentOwner.getName()), String.class).accept(MediaType.TEXT_PLAIN).retrieve().bodyToMono(String.class).block();
    }

    @Override
    public String promoteAdmin(String orgName, Principal currentUser, String targetUserName) {
        String promoteAdminUrl = createPromoteUrl(orgName, targetUserName, "admin");
        var ret = this.webClient.post().uri(promoteAdminUrl).body(Mono.just(currentUser.getName()), String.class).accept(MediaType.TEXT_PLAIN).retrieve().bodyToMono(String.class).block();

        var eventData = new Event.EventData();
        eventData.put("USER", targetUserName);
        String eventMessage = "Promoted user to admin role";
        Event event = new Event.Builder()
                .eventCategory("ORGS")
                .eventType("PROMOTE_USER")
                .message(eventMessage)
                .eventData(eventData)
                .build();
        eventLogger.logWithRemoteUser(event);

        return ret;
    }

    private String createPromoteUrl(String orgName, String targetUserName, String role) {
        return format("%s/api/groups/%s/role/%s/%s",
                clusterProperties.getAsgardUrl(),
                orgName,
                role,
                targetUserName);
    }

    @Override
    public String demoteAdmin(String orgName, Principal currentUser, String targetUserName) {
        String demoteAdminUrl = createPromoteUrl(orgName, targetUserName, "admin");
        var ret = this.webClient.post().uri(demoteAdminUrl).body(Mono.just(currentUser.getName()), String.class).accept(MediaType.TEXT_PLAIN).retrieve().bodyToMono(String.class).block();

        var eventData = new Event.EventData();
        eventData.put("USER", targetUserName);
        String eventMessage = "Removed user from admin role";
        Event event = new Event.Builder()
                .eventCategory("ORGS")
                .eventType("DEMOTE_USER")
                .message(eventMessage)
                .eventData(eventData)
                .build();
        eventLogger.logWithRemoteUser(event);

        return ret;
    }

    @Override
    public String invite(String orgName, String targetEmail, Principal user) {
        String inviteUserUrl = createInviteUserUrl(orgName, targetEmail);
        String sendingUser = user.getName();
        var ret = this.webClient.post().uri(inviteUserUrl).body(Mono.just(sendingUser), String.class).accept(MediaType.TEXT_PLAIN).retrieve().bodyToMono(String.class).block();

        var eventData = new Event.EventData();
        eventData.put("USER", targetEmail);
        String eventMessage = "Invited user";
        Event event = new Event.Builder()
                .eventCategory("ORGS")
                .eventType("INVITE_USER")
                .message(eventMessage)
                .eventData(eventData)
                .build();
        eventLogger.logWithRemoteUser(event);

        return ret;
    }

    public String createInviteUserUrl(String orgName, String targetEmail) {
        return format("%s/api/invites/%s/user/%s", clusterProperties.getAsgardUrl(), orgName, targetEmail);
    }


    @Override
    public String deleteInvite(String orgName, String targetEmail, Principal user) {
        String deleteInviteUrl = createInviteUserUrl(orgName, targetEmail);
        String sendingUser = user.getName();
        var ret = this.webClient.method(HttpMethod.DELETE).uri(deleteInviteUrl).body(Mono.just(sendingUser), String.class).accept(MediaType.TEXT_PLAIN).retrieve().bodyToMono(String.class).block();

        var eventData = new Event.EventData();
        eventData.put("USER", targetEmail);
        String eventMessage = "Removed invite";
        Event event = new Event.Builder()
                .eventCategory("ORGS")
                .eventType("DELETE_INVITE")
                .message(eventMessage)
                .eventData(eventData)
                .build();
        eventLogger.logWithRemoteUser(event);

        return ret;
    }

    @Override
    public String removeUser(String orgName, String targetEmail, Principal user) {
        String removeUserUrl = createGroupUsersUrl(orgName, targetEmail);
        String sendingUser = user.getName();
        var ret = this.webClient.method(HttpMethod.DELETE).uri(removeUserUrl).body(Mono.just(sendingUser), String.class).accept(MediaType.TEXT_PLAIN).retrieve().bodyToMono(String.class).block();

        var eventData = new Event.EventData();
        eventData.put("USER", targetEmail);
        String eventMessage = "Removed user";
        Event event = new Event.Builder()
                .eventCategory("ORGS")
                .eventType("REMOVE_USER")
                .message(eventMessage)
                .eventData(eventData)
                .build();
        eventLogger.logWithRemoteUser(event);

        return ret;
    }

    private String createGroupUsersUrl(String orgName, String targetEmail) {
        return format("%s/api/groups/%s/users/%s", clusterProperties.getAsgardUrl(), orgName, targetEmail);
    }

    @Override
    public OrganizationSummary getOrgSummary(String orgName, Principal user) {
        String orgSummaryUrl = createGroupsUrl(orgName);
        String sendingUser = user.getName();
        return this.webClient.post().uri(orgSummaryUrl).body(Mono.just(sendingUser), String.class).accept(MediaType.APPLICATION_JSON).retrieve().bodyToMono(OrganizationSummary.class).block();
    }

    private String createGroupsUrl(String orgName) {
        return format("%s/api/groups/%s/summary", clusterProperties.getAsgardUrl(), orgName);
    }

}
