package io.openraven.services.google;

import io.openraven.gcp.auth.GoogleConfigSettings;
import io.openraven.models.GoogleWorkspaceConfigSettings;
import io.openraven.models.ScanSource;
import io.openraven.services.GlobalConfigurationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class GoogleWorkspaceSettingsService {
    protected static final String SCAN_SOURCE = "google-workspace.scanSource";
    protected static final String AWS_ACCOUNT_ID = "google-workspace.awsAccountId";
    public static final String AWS_REGION = "google-workspace.awsRegion";
    public static final String GCP_REGION = "google-workspace.gcpRegion";
    public static final String SCAN_BUDGET = "google-workspace.scanBudget";
    public static final String AUTO_SCAN_DATA_COLLECTIONS = "google-workspace.autoScanDataCollections";
    public static final String QUARANTINE_TEXT = "google-workspace.quarantineText";
    public static final String ALLOWED_DOMAINS = "google-workspace.allowedDomains";
    public static final String DENIED_DOMAINS = "google-workspace.deniedDomains";
    public static final String SCAN_EXT_OWNED_ENABLED = "google-workspace.scan-externally-owned-enabled";
    private final GoogleConfigSettings googleConfigSettings;
    private final GlobalConfigurationService globalConfigurationService;

    public GoogleWorkspaceSettingsService(GoogleConfigSettings googleConfigSettings,
                                          GlobalConfigurationService globalConfigurationService) {
        this.googleConfigSettings = googleConfigSettings;
        this.globalConfigurationService = globalConfigurationService;
    }

    public void saveAll(GoogleWorkspaceConfigSettings request) {
        String superUserEmail = request.getSuperUserEmail();
        if (StringUtils.isNotBlank(superUserEmail)) {
            googleConfigSettings.setGoogleWorkspaceSuperUserEmail(superUserEmail);
        }
        globalConfigurationService.writeEnumValue(SCAN_SOURCE, request.getScanSource());
        globalConfigurationService.writeTextValue(AWS_ACCOUNT_ID, request.getAwsAccountId());
        globalConfigurationService.writeTextValue(AWS_REGION, request.getAwsRegion());
        globalConfigurationService.writeTextValue(GCP_REGION, request.getGcpRegion());
        globalConfigurationService.writeLongValue(SCAN_BUDGET, request.getScanBudget());
        globalConfigurationService.writeListValue(AUTO_SCAN_DATA_COLLECTIONS, request.getAutoScanDataCollections());
        globalConfigurationService.writeTextValue(QUARANTINE_TEXT, request.getQuarantineText());
        globalConfigurationService.writeListValue(ALLOWED_DOMAINS, request.getAllowedDomains());
        globalConfigurationService.writeListValue(DENIED_DOMAINS, request.getDeniedDomains());
        globalConfigurationService.writeBooleanValue(SCAN_EXT_OWNED_ENABLED, request.isExternalSharingScanEnabled());
    }

    public GoogleWorkspaceConfigSettings fetch() {
        return new GoogleWorkspaceConfigSettings(
                googleConfigSettings.getGoogleWorkspaceSuperUserEmail(),
                globalConfigurationService.fetchEnumValue(SCAN_SOURCE, ScanSource.class),
                globalConfigurationService.fetchTextValue(AWS_ACCOUNT_ID),
                globalConfigurationService.fetchTextValue(AWS_REGION),
                globalConfigurationService.fetchTextValue(GCP_REGION),
                globalConfigurationService.fetchLongValue(SCAN_BUDGET),
                globalConfigurationService.fetchListValue(AUTO_SCAN_DATA_COLLECTIONS, UUID.class),
                globalConfigurationService.fetchTextValue(QUARANTINE_TEXT),
                globalConfigurationService.fetchListValue(ALLOWED_DOMAINS, String.class),
                globalConfigurationService.fetchListValue(DENIED_DOMAINS, String.class),
                globalConfigurationService.fetchBooleanValue(SCAN_EXT_OWNED_ENABLED)
        );
    }
}
