package io.openraven.services.google;

import io.openraven.gcp.auth.GoogleConfigSettings;
import io.openraven.models.ConfigureServiceAccountImpersonationRequest;
import io.openraven.properties.ClusterProperties;
import io.openraven.properties.zookeeper.CloudIngestionProperties;
import io.openraven.services.GlobalConfigurationService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import static java.lang.String.format;

@Component
public class GcpProjectService {
    public static final String GCP_OPENRAVEN_PROJECT_ID_KEY = "gcp.openravenProjectId";
    private final WebClient webClient;
    private final String clusterId;
    private final String asgardUrl;
    private final GlobalConfigurationService globalConfigurationService;
    private final GoogleConfigSettings googleConfigSettings;

    public GcpProjectService(WebClient webClient, CloudIngestionProperties cloudIngestionProperties,
                             ClusterProperties clusterProperties,
                             GlobalConfigurationService globalConfigurationService,
                             GoogleConfigSettings googleConfigSettings) {
        this.clusterId = cloudIngestionProperties.getAnalytics().getClusterId();
        this.webClient = webClient;
        this.asgardUrl = format("%s/api/enrollment/gcp/configureServiceAccountImpersonation", clusterProperties.getAsgardUrl());
        this.globalConfigurationService = globalConfigurationService;
        this.googleConfigSettings = googleConfigSettings;
    }

    public void connectProject(String projectId, String serviceAccountEmail) {
        var request = new ConfigureServiceAccountImpersonationRequest(clusterId, serviceAccountEmail);
        webClient.post()
                .uri(asgardUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(request))
                .retrieve()
                .toBodilessEntity()
                .block();
        this.globalConfigurationService.writeTextValue(GCP_OPENRAVEN_PROJECT_ID_KEY, projectId);
        this.googleConfigSettings.setScannerServiceAccountEmail(serviceAccountEmail);
    }

    public String setGoogleWorkspaceSuperAdmin(String superAdminEmail) {
        this.googleConfigSettings.setGoogleWorkspaceSuperUserEmail(superAdminEmail);
        return superAdminEmail;
    }

    public String getGoogleWorkspaceSuperAdmin() {
        return this.googleConfigSettings.getGoogleWorkspaceSuperUserEmail();
    }
}
