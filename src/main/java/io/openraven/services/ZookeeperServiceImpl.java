package io.openraven.services;

import io.openraven.models.DataScanSettings;
import io.openraven.properties.zookeeper.ScannerProperties;
import io.openraven.properties.zookeeper.models.SchedulerPropertiesModel;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.imps.CuratorFrameworkState;
import org.apache.zookeeper.KeeperException;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.zookeeper.config.ZookeeperConfigProperties;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.nio.charset.StandardCharsets;

import static io.openraven.services.ZookeeperService.combine;
import static java.lang.String.format;
import static org.apache.commons.codec.binary.StringUtils.getBytesUtf8;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;

// Need to provide a different bean name because of ZookeeperServiceImpl in gcp-auth.
// Once ZK stuff is removed from that lib, this explicit name is no longer needed.
@Component("applicationZookeeperService")
public class ZookeeperServiceImpl implements ZookeeperService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ZookeeperServiceImpl.class);
    private final CuratorFramework zookeeperClient;
    private final ZookeeperConfigProperties zookeeperConfigProperties;
    private final ScannerProperties scannerProperties;

    // S3 Scan Properties
    private static final String S3_SCAN_MAXIMUM_FILE_SIZE_TO_SCAN_LOCATION =
            combine(ScannerProperties.ZK_PATH, "maximumFileSizeToScan");
    private static final String S3_SCAN_ENABLED_LOCATION =
            combine(ScannerProperties.ZK_PATH, "s3Activation");

    // Data Scanner keys
    private static final String DATA_SCANNER_ACTIVATION_LOCATION = combine(SchedulerPropertiesModel.ZK_PATH, "enabled");
    private static final String DATA_SCANNER_FILES_LOCATION = combine(SchedulerPropertiesModel.ZK_PATH, "files");
    private static final String DATA_SCANNER_BUCKETS_LOCATION = combine(SchedulerPropertiesModel.ZK_PATH, "buckets");

    public ZookeeperServiceImpl(
            CuratorFramework zookeeperClient,
            ZookeeperConfigProperties zookeeperConfigProperties,
            ScannerProperties scannerProperties) {
        this.zookeeperClient = zookeeperClient;
        this.zookeeperConfigProperties = zookeeperConfigProperties;
        this.scannerProperties = scannerProperties;
    }


    @Override
    public void setDataScanSettings(DataScanSettings dataScanningSettings) {
        writeToZookeeper(DATA_SCANNER_ACTIVATION_LOCATION, dataScanningSettings.activation);
        writeToZookeeper(DATA_SCANNER_FILES_LOCATION, dataScanningSettings.files);
        writeToZookeeper(DATA_SCANNER_BUCKETS_LOCATION, dataScanningSettings.buckets);
        var allowedMaximumFileSize = scannerProperties.getMaximumFileSizeUpperLimit();
        var requestedMaximumFileSize = dataScanningSettings.maximumFileSizeToScan;
        if (requestedMaximumFileSize <= allowedMaximumFileSize) {
            writeToZookeeper(S3_SCAN_MAXIMUM_FILE_SIZE_TO_SCAN_LOCATION, requestedMaximumFileSize);
        } else {
            writeToZookeeper(S3_SCAN_MAXIMUM_FILE_SIZE_TO_SCAN_LOCATION, allowedMaximumFileSize);
            LOGGER.warn("Attempt to set maximum file size to {} MB, but overridden with allowed maximum of {} MB", requestedMaximumFileSize, allowedMaximumFileSize);
        }
        writeToZookeeper(S3_SCAN_ENABLED_LOCATION, dataScanningSettings.s3Activation);
    }

    @Override
    public DataScanSettings getDataScanSettings() {
        int allowedMaximumFileSize = Integer.parseInt(readFromZookeeper(ScannerProperties.ZK_PATH + ScannerProperties.MAXIMUM_FILE_SIZE_UPPER_LIMIT_NAME, "8192"));
        if (scannerProperties.getMaximumFileSizeToScan() > allowedMaximumFileSize) {
            scannerProperties.setMaximumFileSizeToScan(allowedMaximumFileSize);
            writeToZookeeper(S3_SCAN_MAXIMUM_FILE_SIZE_TO_SCAN_LOCATION, allowedMaximumFileSize);
        }
        return new DataScanSettings(createSchedulerPropertiesModel(), createS3ScanPropertiesFromZK());
    }

    @NotNull
    private SchedulerPropertiesModel createSchedulerPropertiesModel() {
        var retVal = new SchedulerPropertiesModel();
        retVal.initialize( this);
        return retVal;
    }

    @NotNull
    private ScannerProperties createS3ScanPropertiesFromZK() {
        this.scannerProperties.initialize(this);

        return scannerProperties;
    }

    public <T> void writeToZookeeper(final String path, final T payload) {
        LOGGER.debug("writing {} to ZK path {}", payload, path);
        try {
            byte[] bytes = payload != null ? getBytesUtf8(format("%s", payload)) : null;
            writeBytesToZookeeper(path, bytes);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static final String ROLE_ARN_LOCATION = "openraven/accounts/crossaccount/rolearns";

    @Override
    public int getRoleArnsSize() {
        final var roleCsv = readFromZookeeper(ROLE_ARN_LOCATION, "");
        if (StringUtils.hasText(roleCsv)) {
            final var arns = roleCsv.split("\\s*,\\s*");
            return arns.length;
        } else {
            return 0;
        }
    }

    private void writeBytesToZookeeper(final String path, final byte[] bytes) throws Exception {
        LOGGER.debug("Zookeeper CuratorFramework client state {}", zookeeperClient.getState());
        if (zookeeperClient.getState() != CuratorFrameworkState.STARTED) {
            LOGGER.info("Starting Zookeeper CuratorFramework");
            zookeeperClient.start();
        }
        String location = path;

        if (!StringUtils.startsWithIgnoreCase(path, combineRootAndContext())) {
            location = combine(combineRootAndContext(), path);
        }

        this.zookeeperClient.createContainers(location);
        this.zookeeperClient.setData().forPath(location, bytes);
    }

    private String combineRootAndContext() {
        String defaultContext = zookeeperConfigProperties.getDefaultContext();
        String root = zookeeperConfigProperties.getRoot();
        return combine("", root, defaultContext);
    }

    @Override
    public String readFromZookeeper(String location, String defaultValue) {

        if (!StringUtils.startsWithIgnoreCase(location, combineRootAndContext())) {
            location = combine(combineRootAndContext(), location);
        }
        try {
            final byte[] data = zookeeperClient.getData().forPath(location);
            if (isNotEmpty(data)) {
                return new String(data, StandardCharsets.UTF_8);
            }
        } catch (KeeperException.NoNodeException ignored) {
        } catch (Exception e) {
            LOGGER.warn("Exception thrown when getRoleArnsSize called", e);
            throw new RuntimeException(e);
        }
        return defaultValue;
    }
}
