package io.openraven.services.office365;

import com.microsoft.aad.msal4j.ClientCredentialFactory;
import com.microsoft.aad.msal4j.ClientCredentialParameters;
import com.microsoft.aad.msal4j.ConfidentialClientApplication;
import com.microsoft.graph.concurrency.ICallback;
import com.microsoft.graph.core.ClientException;
import com.microsoft.graph.models.extensions.Attachment;
import com.microsoft.graph.models.extensions.FileAttachment;
import com.microsoft.graph.models.extensions.IGraphServiceClient;
import com.microsoft.graph.models.extensions.MailFolder;
import com.microsoft.graph.models.extensions.Message;
import com.microsoft.graph.models.extensions.User;
import com.microsoft.graph.options.Option;
import com.microsoft.graph.options.QueryOption;
import com.microsoft.graph.requests.extensions.GraphServiceClient;
import com.microsoft.graph.requests.extensions.IAttachmentCollectionPage;
import com.microsoft.graph.requests.extensions.IMessageCollectionPage;
import com.microsoft.graph.requests.extensions.IUserCollectionPage;
import io.openraven.models.Office365ConfigurationData;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static java.lang.String.format;

@Component
public class OutlookService {

    public static final String AUTHORITY_ROOT_FORMAT = "https://login.microsoftonline.com/%s/";

    @Async
    public CompletableFuture<IGraphServiceClient> getMSAL(
            Office365ConfigurationData office365Configuration) throws IOException {

        // Check configuration
        if (StringUtils.isEmpty(office365Configuration.clientId))
            throw new IOException("\"clientId\" missing from application.yml");

        if (StringUtils.isEmpty(office365Configuration.clientSecret))
            throw new IOException("\"clientSecret\" missing from application.yml");

        if (StringUtils.isEmpty(office365Configuration.defaultScope))
            throw new IOException("\"defaultScope\" missing from application.yml");

        if (StringUtils.isEmpty(office365Configuration.tenantAuthority))
            throw new IOException("\"tenantAuthority\" missing from application.yml");

        CompletableFuture<IGraphServiceClient> clientFuture = new CompletableFuture<>();

        ConfidentialClientApplication app = ConfidentialClientApplication
                .builder(office365Configuration.clientId,
                        ClientCredentialFactory.createFromSecret(office365Configuration.clientSecret))
                .authority(format(AUTHORITY_ROOT_FORMAT,office365Configuration.tenantAuthority)).build();

        ClientCredentialParameters clientCredentialParam = ClientCredentialParameters
                .builder(Collections.singleton(office365Configuration.defaultScope)).build();


        app.acquireToken(clientCredentialParam).whenCompleteAsync((res, ex) -> {
            if (ex != null) {
                clientFuture.completeExceptionally(ex);
            }

            IGraphServiceClient graphClient = GraphServiceClient.builder()
                    .authenticationProvider(new SimpleAuthProvider(res.accessToken())).buildClient();

            clientFuture.complete(graphClient);
        }).join();


        return clientFuture;


    }

    @Async
    public CompletableFuture<List<User>> getUsers(IGraphServiceClient iGraphServiceClient) {
        CompletableFuture<List<User>> listUserCompletable = new CompletableFuture<>();
        iGraphServiceClient.users().buildRequest().get(new ICallback<IUserCollectionPage>() {
            @Override
            public void success(IUserCollectionPage iUserCollectionPage) {
                listUserCompletable.complete(iUserCollectionPage.getCurrentPage());
            }

            @Override
            public void failure(ClientException e) {
                listUserCompletable.completeExceptionally(e);
            }
        });
        return listUserCompletable;
    }

    public static MailFolder getUserMailFolder(IGraphServiceClient iGraphServiceClient, String userId,
                                               String folderName) {
        return iGraphServiceClient.users(userId).mailFolders(folderName).buildRequest().get();
    }

    public static List<Message> getUserFolderMessages(IGraphServiceClient iGraphServiceClient,
                                                      String userId, String folderId, String queries) {
        List<Option> requestOption = new ArrayList<>();
        requestOption.add(new QueryOption("$filter", queries));
        IMessageCollectionPage iMessageCollectionPage = iGraphServiceClient.users(userId)
                .mailFolders(folderId).messages().buildRequest(requestOption).get();
        List<Message> messages = new ArrayList<>(iMessageCollectionPage.getCurrentPage());
        while (iMessageCollectionPage.getNextPage() != null) {
            iMessageCollectionPage = iMessageCollectionPage.getNextPage().buildRequest().get();
            messages.addAll(iMessageCollectionPage.getCurrentPage());
        }
        return messages;
    }

    public static List<Attachment> getMessageAttachments(IGraphServiceClient iGraphServiceClient,
                                                         String userId, String folderId, String mailId) {
        IAttachmentCollectionPage iAttachmentCollectionPage = iGraphServiceClient.users(userId)
                .mailFolders(folderId).messages(mailId).attachments().buildRequest().get();
        List<Attachment> attachments = new ArrayList<>(iAttachmentCollectionPage.getCurrentPage());
        while (iAttachmentCollectionPage.getNextPage() != null) {
            iAttachmentCollectionPage = iAttachmentCollectionPage.getNextPage().buildRequest().get();
            attachments.addAll(iAttachmentCollectionPage.getCurrentPage());
        }
        return attachments;
    }

    public static FileAttachment getAttachment(IGraphServiceClient iGraphServiceClient, String userId,
                                               String mailId, String attachmentId) {
        return (FileAttachment) iGraphServiceClient.users(userId).messages(mailId)
                .attachments(attachmentId).buildRequest().get();
    }
}
