package io.openraven.services;

import io.openraven.gcp.auth.GoogleConfigSettings;
import io.openraven.services.google.GcpProjectService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

@Service
@Order(GcpSettingMigrationApplicationRunner.ORDER)
@Profile("gcp")
public class GcpSettingMigrationApplicationRunner implements ApplicationRunner {
    public static final int ORDER = 11;

    private static final String ZK_PATH = "openraven/app/v1/cloudstorage/config";

    private final ZookeeperService zookeeperService;
    private final GoogleConfigSettings googleConfigSettings;
    private final GlobalConfigurationService globalConfigurationService;

    public GcpSettingMigrationApplicationRunner(ZookeeperService zookeeperService,
                                                GoogleConfigSettings googleConfigSettings,
                                                GlobalConfigurationService globalConfigurationService) {
        this.zookeeperService = zookeeperService;
        this.googleConfigSettings = googleConfigSettings;
        this.globalConfigurationService = globalConfigurationService;
    }


    /**
     * If GCP settings aren't already in the globalConfiguration table in the database,
     * this looks for those values in their legacy Zookeeper locations and copies them to the database.
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (googleConfigSettings.getScannerServiceAccountEmail() == null) {
            String serviceAccountEmail = this.zookeeperService.readFromZookeeper(ZK_PATH + "/projectLocalServiceAccountEmail", null);
            if (serviceAccountEmail != null) {
                googleConfigSettings.setScannerServiceAccountEmail(serviceAccountEmail);
            }
        }

        if (globalConfigurationService.fetchTextValue(GcpProjectService.GCP_OPENRAVEN_PROJECT_ID_KEY) == null) {
            String projectId = zookeeperService.readFromZookeeper(ZK_PATH + "/openRavenProjectId", null);
            globalConfigurationService.writeTextValue(GcpProjectService.GCP_OPENRAVEN_PROJECT_ID_KEY, projectId);
        }
    }
}
