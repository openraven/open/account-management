package io.openraven.services;

import io.openraven.gcp.auth.GoogleConfigSettings;
import io.openraven.gcp.auth.model.CredentialsConfig;
import io.openraven.properties.ClusterProperties;
import io.openraven.properties.zookeeper.CloudIngestionProperties;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sts.StsClient;

import static java.lang.String.format;

@Service
@Order(GcpEnrollmentApplicationRunner.ORDER)
@Profile("gcp")
public class GcpEnrollmentApplicationRunner implements ApplicationRunner {

    public static final int ORDER = 10;

    private final WebClient webClient;
    private final GoogleConfigSettings googleConfigSettings;
    private final String enrollmentApiUrl;

    public GcpEnrollmentApplicationRunner(WebClient webClient,
                                          GoogleConfigSettings googleConfigSettings,
                                          ClusterProperties clusterProperties,
                                          CloudIngestionProperties cloudIngestionProperties) {
        this.webClient = webClient;
        this.googleConfigSettings = googleConfigSettings;
        try (var client = StsClient.builder().region(Region.AWS_GLOBAL).build()) {
            var awsAccount = client.getCallerIdentity().account();
            this.enrollmentApiUrl = format("%s/api/enrollment/gcp/%s/%s", clusterProperties.getAsgardUrl(), cloudIngestionProperties.getAnalytics().getClusterId(), awsAccount);
        }
    }

    /**
     * Uses our oauth2 configured webclient to make an authenticated request to the asgard url to generate a credentials
     * configuration for storage in globalConfiguration. This makes the credential configuration available for consumption in
     * downstream services that have a dependency on gcp-auth-service.
     * @param args incoming application arguments, not configured
     * @throws Exception - Can throw an exception during the api call if a non 200 reply comes back
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {
        var credentialsConfig = webClient.post()
                .uri(enrollmentApiUrl)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(CredentialsConfig.class)
                .block();

        googleConfigSettings.setCredentialsConfiguration(credentialsConfig);
    }
}