package io.openraven.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.fasterxml.jackson.databind.node.LongNode;
import com.fasterxml.jackson.databind.node.TextNode;
import io.openraven.gcp.auth.model.jpa.GlobalConfigurationEntity;
import io.openraven.gcp.auth.repository.GlobalConfigurationRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GlobalConfigurationService {
    private final GlobalConfigurationRepository globalConfigurationRepository;
    private final ObjectMapper objectMapper;

    public GlobalConfigurationService(GlobalConfigurationRepository globalConfigurationRepository,
                                      ObjectMapper objectMapper) {
        this.globalConfigurationRepository = globalConfigurationRepository;
        this.objectMapper = objectMapper;
    }

    public Optional<JsonNode> fetchValue(String key) {
        return globalConfigurationRepository.findById(key).map(GlobalConfigurationEntity::getValue);
    }

    public String fetchTextValue(String key) {
        return fetchValue(key).map(JsonNode::asText).orElse(null);
    }

    public Boolean fetchBooleanValue(String key) {
        return fetchValue(key).map(JsonNode::asBoolean).orElse(null);
    }

    public Long fetchLongValue(String key) {
        return fetchValue(key).map(JsonNode::asLong).orElse(null);
    }

    public <E extends Enum<E>> E fetchEnumValue(String key, Class<E> elementType) {
        return fetchValue(key).map(n -> Enum.valueOf(elementType, n.asText())).orElse(null);
    }

    public <T> List<T> fetchListValue(String key, Class<T> elementType) {
        return fetchValue(key).map(n -> treeToList(n, elementType)).orElse(null);
    }

    private <T> List<T> treeToList(JsonNode node, Class<T> elementType) {
        var valueType= objectMapper.getTypeFactory().constructCollectionType(List.class, elementType);
        try {
            return objectMapper.treeToValue(node, valueType);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public void writeListValue(String key, List<?> value) {
        if (value != null) {
            writeJsonNode(key, objectMapper.valueToTree(value));
        }
    }

    public void writeTextValue(String key, String value) {
        if (value != null) {
            writeJsonNode(key, new TextNode(value));
        }
    }

    public void writeLongValue(String key, Long value) {
        if (value != null) {
            writeJsonNode(key, new LongNode(value));
        }
    }

    public void writeEnumValue(String key, Enum<?> value) {
        if (value != null) {
            writeJsonNode(key, new TextNode(value.name()));
        }
    }

    public void writeJsonNode(String key, JsonNode value) {
        globalConfigurationRepository.saveAndFlush(new GlobalConfigurationEntity(key, value));
    }

    public void writeTextValueIfAbsent(String key, String value) {
        if (!globalConfigurationRepository.existsById(key)) {
            writeTextValue(key, value);
        }
    }

    public void writeLongValueIfAbsent(String key, Long value) {
        if (!globalConfigurationRepository.existsById(key)) {
            writeLongValue(key, value);
        }
    }

    public void writeListValueIfAbsent(String key, List<?> value) {
        if (!globalConfigurationRepository.existsById(key)) {
            writeListValue(key, value);
        }
    }

    public void writeBooleanValue(String key, Boolean value) {
        if (value != null) {
            writeJsonNode(key, BooleanNode.valueOf(value));
        }
    }
}
