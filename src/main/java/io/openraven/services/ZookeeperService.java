package io.openraven.services;

import io.openraven.models.DataScanSettings;

import static java.lang.String.join;

public interface ZookeeperService {

    void setDataScanSettings(DataScanSettings dataScanningSettings);

    DataScanSettings getDataScanSettings();

    /**
     * Calls, conceptually, {@code toString} on {@code payload} and writes it to the ZK path provided,
     * with the caveat noted on the {@code path} param.
     *
     * @param path the (possibly relative) ZK path to which the UTF-8 bytes of the {@code toString} of {@code payload} will be written,
     *             and {@code path} will be prefixed with {@code combineRootAndContext} if it doesn't already start with it.
     */
    <T> void writeToZookeeper(final String path, final T payload) throws RuntimeException;

    static String combine(String... paths) {
        return join("/", paths);
    }

    int getRoleArnsSize();

    String readFromZookeeper(String location, String defaultValue);
}
