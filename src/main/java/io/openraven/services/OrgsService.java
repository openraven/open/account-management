package io.openraven.services;

import io.openraven.models.OrganizationParticipation;
import io.openraven.models.OrganizationSummary;
import io.openraven.models.User;
import org.springframework.security.core.Authentication;

import java.security.Principal;
import java.util.List;

public interface OrgsService {

    List<OrganizationParticipation> listOrgs(Authentication user);

    String deleteOrg(String orgName, Principal user);

    String promoteOwner(String orgName, Principal currentOwner, String targetUserName);

    String promoteAdmin(String orgName, Principal currentUser, String targetUserName);

    String demoteAdmin(String orgName, Principal currentUser, String targetUserName);

    String invite(String orgName, String targetEmail, Principal user);

    String deleteInvite(String orgName, String targetEmail, Principal user);

    String removeUser(String orgName, String targetEmail, Principal user);

    OrganizationSummary getOrgSummary(String orgName, Principal user);

    User[] listUsers(String orgName, Principal user);

    User fetchUser(Principal user);
}
