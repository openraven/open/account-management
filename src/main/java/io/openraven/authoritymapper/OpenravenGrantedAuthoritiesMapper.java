package io.openraven.authoritymapper;

import io.openraven.properties.ClusterProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.join;
import static java.util.stream.Collectors.toList;
import static org.springframework.util.StringUtils.hasText;

@Component
public class OpenravenGrantedAuthoritiesMapper implements Converter<Jwt, AbstractAuthenticationToken>, GrantedAuthoritiesMapper {
    private static final Logger LOG = LoggerFactory.getLogger(OpenravenGrantedAuthoritiesMapper.class);
    private static final String ADMINS = "admins";
    private static final String OWNERS = "owners";
    private static final SimpleGrantedAuthority ROLE_OWNER = new SimpleGrantedAuthority("ROLE_owner");
    private static final SimpleGrantedAuthority ROLE_USER = new SimpleGrantedAuthority("ROLE_user");
    private static final SimpleGrantedAuthority ROLE_ADMIN = new SimpleGrantedAuthority("ROLE_admin");
    private static final String SCOPE_AUTHORITY_PREFIX = "SCOPE_";
    private static final Collection<String> WELL_KNOWN_SCOPE_ATTRIBUTE_NAMES =
            Arrays.asList("scope", "scp");
    private final ClusterProperties clusterProperties;

    public OpenravenGrantedAuthoritiesMapper(ClusterProperties clusterProperties) {
        this.clusterProperties = clusterProperties;
    }

    @Override
    public Collection<GrantedAuthority> mapAuthorities(Collection<? extends GrantedAuthority> authorities) {
        final String name = clusterProperties.getName();
        final String parent = clusterProperties.getParent();
        final String separator = clusterProperties.getSeparator();
        final String baseName = join("", separator, parent, separator, name);
        final String adminsGroupName = join("", baseName, separator, name, "-", ADMINS);
        final String ownersGroupName = join("", baseName, separator, name, "-", OWNERS);
        return authorities.stream().map((authority) ->
                getGrantedAuthority(baseName, adminsGroupName, ownersGroupName, authority, authority.getAuthority())
        ).collect(toList());
    }

    private GrantedAuthority getGrantedAuthority(String baseName, String adminsGroupName, String ownersGroupName, GrantedAuthority authority, String authorityString) {
        if (authorityString.equals(baseName)) {
            return ROLE_USER;
        }
        if (authorityString.equals(adminsGroupName)) {
            return ROLE_ADMIN;
        }
        if (authorityString.equals(ownersGroupName)) {
            return ROLE_OWNER;
        }
        return authority;
    }

    @Override
    public AbstractAuthenticationToken convert(@NonNull Jwt jwt) {
        Collection<GrantedAuthority> authorities = extractAuthorities(jwt);
        LOG.debug("Groups on the token are {}", authorities);
        LOG.debug("Type value of token is {}", jwt.getClaims().get("jti"));
        return new JwtAuthenticationToken(jwt, authorities);
    }


    protected Collection<GrantedAuthority> extractAuthorities(Jwt jwt) {
        return mapAuthorities(Stream.concat(this.getScopes(jwt).map(authority -> SCOPE_AUTHORITY_PREFIX + authority),
                this.getGroups(jwt))
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList()));
    }

    @SuppressWarnings("unchecked")
    private Stream<String> getGroups(Jwt jwt) {
        Object scopes = jwt.getClaims().get("groups");
        if (scopes instanceof Collection) {
            return ((Collection<String>) scopes).stream();
        }
        return Stream.empty();
    }


    @SuppressWarnings("unchecked")
    private Stream<String> getScopes(Jwt jwt) {
        for (String attributeName : WELL_KNOWN_SCOPE_ATTRIBUTE_NAMES) {
            Object scopes = jwt.getClaims().get(attributeName);
            if (scopes instanceof String) {
                if (hasText((String) scopes)) {
                    return Stream.of(((String) scopes).split("\\s"));
                } else {
                    return Stream.empty();
                }
            } else if (scopes instanceof Collection) {
                return ((Collection<String>) scopes).stream();
            }
        }

        return Stream.empty();
    }


}
