package io.openraven.integration;

import io.openraven.data.TestOrganizationalUnitBuilder;
import io.openraven.gcp.auth.GoogleConfigSettings;
import io.openraven.gcp.auth.model.CredentialsConfig;
import io.openraven.services.ZookeeperService;
import io.openraven.simulators.OdinSimulator;
import io.openraven.testcontainers.MotoContainer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.reactive.function.client.WebClient;
import org.testcontainers.junit.jupiter.Container;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(
        properties = {
                IntegrationTestConstants.DATABASE_TAG_PROPERTY,
                IntegrationTestConstants.ZOOKEEPER_IMAGE_PROPERTY,
        }
)
@ActiveProfiles({"default", "gcp", "integration-test"})
@AutoConfigureMockMvc
public class SmokeIT {

    @Autowired
    ApplicationContext ctx;

    @Autowired
    MockMvc mockMvc;
    @Autowired
    ZookeeperService zookeeperService;

    @MockBean
    WebClient webClient;

    @Autowired
    MockingApplicationRunner mockingApplicationRunner;

    @Container
    private static final MotoContainer moto = new MotoContainer(IntegrationTestConstants.MOTO_TAG);

    @Autowired
    GoogleConfigSettings googleConfigSettings;


    @BeforeAll
    public static void before() {
        moto.start();
        var odinSim = new OdinSimulator("00g1il2zjj75suK9Q0h8", moto.getAwsEndpoint());
        odinSim.createOrganization("555555555003", rootOu -> rootOu
                .createOrganizationalUnit(TestOrganizationalUnitBuilder::createAccountWithDiscoveryRole)
                .createAccountWithDiscoveryRole()
        );
    }

    @Test
    public void ctxComesUp() throws Exception {
        assertNotNull(ctx);
        CredentialsConfig expected = mockingApplicationRunner.getCredentialsConfig();

        var actual = googleConfigSettings.getCredentialsConfiguration();

        assertEquals(expected, actual);
    }
}