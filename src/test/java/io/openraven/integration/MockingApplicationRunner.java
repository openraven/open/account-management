package io.openraven.integration;

import io.openraven.gcp.auth.model.CredentialsConfig;
import io.openraven.services.GcpEnrollmentApplicationRunner;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Order(GcpEnrollmentApplicationRunner.ORDER - 1)
@Service
@Profile("gcp")
public class MockingApplicationRunner implements ApplicationRunner {

    private WebClient mockWebClient;
    private final CredentialsConfig credentialsConfig = new CredentialsConfig("type", "audience", "subecttokentype", "servicaccountimpersonationurl", "tokenUrl", new CredentialsConfig.CredentialSource("environmentid", "regionalcredverificationurl"));
    ;

    public MockingApplicationRunner(WebClient webClient) {
        this.mockWebClient = webClient;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        var mockRequestBodyUriSpec = mock(WebClient.RequestBodyUriSpec.class);
        when(mockWebClient.post()).thenReturn(mockRequestBodyUriSpec);
        when(mockRequestBodyUriSpec.uri(any(String.class))).thenReturn(mockRequestBodyUriSpec);
        when(mockRequestBodyUriSpec.accept(MediaType.APPLICATION_JSON)).thenReturn(mockRequestBodyUriSpec);
        var mockResponseSpec = mock(WebClient.ResponseSpec.class);
        when(mockRequestBodyUriSpec.retrieve()).thenReturn(mockResponseSpec);
        when(mockResponseSpec.bodyToMono(CredentialsConfig.class)).thenReturn(Mono.just(credentialsConfig));
    }

    public CredentialsConfig getCredentialsConfig() {
        return credentialsConfig;
    }
}
