package io.openraven.integration;

public interface IntegrationTestConstants {

    String MOTO_TAG = "599655866";
    String DATABASE_TAG = "pg-with-schema.1158369320";
    String ZOOKEEPER_IMAGE = "zookeeper:3.7";

    String DATABASE_TAG_PROPERTY = "openraven.integration-test.database.image-tag=" + DATABASE_TAG;
    String ZOOKEEPER_IMAGE_PROPERTY = "openraven.integration-test.zookeeper.image-name=" + ZOOKEEPER_IMAGE;
}
